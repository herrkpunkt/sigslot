sigslot - C++ Signal/Slot Library
Written by Sarah Thompson - sarah@telergy.com

You can find the latest version of this documentation at http://sigslot.sourceforge.net/

For downloads, mailing lists, bug reports, help and support, or anything similar, go to http://sourceforge.net/projects/sigslot

The author's personal web site is at http://www.findatlantis.com/